#!/usr/bin/env python

from setuptools import setup


setup(
    name='logrng',
    version='0.1.1',
    url='https://bitbucket.org/robmoss/numpy-prng-logger/',
    description='Log each pseudo-random number generator call',
    license='BSD 3-Clause License',
    author='Rob Moss',
    author_email='rgmoss@unimelb.edu.au',
    py_modules=['logrng'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'test-prng = logrng:test_prng'
        ]
    },
    install_requires=[
        'numpy >= 1.7',
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Topic :: Scientific/Engineering :: Mathematics'],
)
