# Log each pseudo-random number generator call

## Description

This package provides a wrapper for the NumPy pseudo-random number generator
that logs each method call.

## Usage

Replace instances of `numpy.random.RandomState` with `logrng.RandomState`:

    >>> import logging
    >>> import logrng
    >>> r = logrng.RandomState(seed=1, level=logging.WARNING)
    >>> r.normal()
    WARNING:logrng.RandomState:normal()
    1.6243453636632417
    >>> r.binomial(n=10, p=0.25, size=5)
    WARNING:logrng.RandomState:binomial(p=0.25, size=5, n=10)
    WARNING:logrng.RandomState:[0, 2, ..., 1, 1]
    array([0, 2, 1, 1, 1])

## Documentation

The documentation can be built locally with
[sphinx](https://pypi.python.org/pypi/Sphinx):

    python setup.py build_sphinx

Note that the [sphinx_rtd_theme](https://github.com/snide/sphinx_rtd_theme/)
theme must be installed.

## License

The code is distributed under the terms of the BSD 3-Clause license (see
`LICENSE`), and the documentation is distributed under the terms of the
[Creative Commons BY-SA 4.0
license](http://creativecommons.org/licenses/by-sa/4.0/).

## Installation

Clone this repository and execute:

    python setup.py install

If you don't have admin rights, install the package locally:

    python setup.py install --user

## Dependencies

This package requires [NumPy](http://www.numpy.org/) >= 1.7.
