"""Log each pseudo-random number generator call."""

from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals

import logging
import numpy as np

__package_name__ = u'logrng'
__author__ = u'Rob Moss'
__email__ = u'rgmoss@unimelb.edu.au'
__copyright__ = u'2015, Rob Moss'
__license__ = u'BSD 3-Clause License'
__version__ = u'0.1.1'


class RandomState(object):
    """A pseudo-random number generator that logs each method call."""

    def __init__(self, seed=None, level=logging.DEBUG):
        """Initialise the pseudo-random number generator.

        :param seed: The random seed for initialising the pseudo-random number
            generator. See the ``numpy.random.RandomState`` documentation for
            a description of the accepted values.
        :param level: The logging level at which the method calls and other
            messages will be generated.
        """
        logging.basicConfig()
        self.__rnd = np.random.RandomState(seed)
        self.__seed = seed
        self.__last_state = self.__rnd.get_state()
        self.__log_name = "{}.{}".format(__name__, self.__class__.__name__)
        self.__logger = logging.getLogger(self.__log_name)
        self.__level = level

    def __log(self, msg, *args, **kwargs):
        """A shorthand function for logging a message."""
        self.__logger.log(self.__level, msg, *args, **kwargs)

    def __getattr__(self, name):
        """Called when an attribute lookup has not found the attribute; the
        lookup is delegated to the underlying pseudo-random number generator
        via a wrapper function that logs the function arguments (if any)."""
        def delegate_call(*args, **kwargs):
            # Record the method call.
            rnd_attr = getattr(self.__rnd, name)
            call = "{}({})".format(
                name,
                ", ".join(
                    [repr(a) for a in args] +
                    ["{}={}".format(k, repr(v)) for k, v in kwargs.items()]
                ))
            self.__log(call)
            # Compare the current PRNG state to the last known state.
            curr_state = self.__rnd.get_state()
            changed_ixs = []
            for ix, val in enumerate(curr_state):
                if isinstance(val, np.ndarray):
                    match = (val == self.__last_state[ix]).all()
                else:
                    match = val == self.__last_state[ix]
                if not match:
                    changed_ixs.append(ix)
            if changed_ixs:
                self.__log("State[{}] has changed".format(changed_ixs))
            # Determine the result of the method call.
            value = rnd_attr(*args, **kwargs)
            # Print a summary of the result, if it is an array.
            if isinstance(value, np.ndarray):
                flat = value.flatten()
                if flat.size > 4:
                    self.__log("[{}, {}, ..., {}, {}]".format(
                        flat[0], flat[1], flat[-2], flat[-1]))
                else:
                    self.__log(flat)
            # Record the updated PRNG state.
            self.__last_state = self.__rnd.get_state()
            # Return the result of the method call.
            return(value)

        # Return the proxy method that wraps the RandomState method call.
        return(delegate_call)


def test_prng(seed=3001, nvec=100, pr=0.40, size=5e7, quiet=False):
    """Calculate the probability density for a large number of random samples
    drawn from a Binomial distribution.

    :param seed: The random seed for initialising the pseudo-random number
        generator. See the ``numpy.random.RandomState`` documentation for a
        description of the accepted values.
    :param nvec: The number of vectors into which to bin the random values.
    :param pr: The probability of success for the Binomial distribution from
        which the random values will be drawn.
    :param size: The number of samples to draw from the Binomial distribution.
    :param quiet: Suppress the printed output, only return the binned values.

    :return: The *probability density* for each bin, if ``quiet=True``.

    As of NumPy 1.9, the state of the PRNG will most likely differ from that
    obtained in NumPy < 1.9 *even* when the same seed is used.

    This function has been observed to produce the following outputs:

    ::

        NumPy version 1.9.2
        [  9.54000000e-06   2.60400000e-05   6.35400000e-05   1.43540000e-04
           3.10820000e-04   6.32080000e-04   1.20060000e-03   2.21072000e-03
           3.83576000e-03   6.35290000e-03   1.00090000e-02   1.50644200e-02
           2.16732600e-02   2.97347600e-02   3.90948800e-02   4.91253400e-02
           5.91625400e-02   6.82294000e-02   7.53733600e-02   7.98474200e-02
           8.12980400e-02   7.92242200e-02   7.42122400e-02   6.66731600e-02
           5.76138000e-02   4.78187800e-02   3.81095200e-02   2.92233200e-02
           2.14376000e-02   1.52141600e-02   1.03501800e-02   6.73734000e-03
           4.23386000e-03   2.56140000e-03   1.47704000e-03   8.27340000e-04
           4.46780000e-04   2.29740000e-04   1.11020000e-04   5.38800000e-05]

    ::

        NumPy version 1.8.2
        [  9.68000000e-06   2.60800000e-05   6.38800000e-05   1.43920000e-04
           3.11660000e-04   6.33580000e-04   1.20380000e-03   2.21470000e-03
           3.84108000e-03   6.36306000e-03   1.00246600e-02   1.50838200e-02
           2.17020600e-02   2.97646800e-02   3.91271600e-02   4.91685200e-02
           5.92135800e-02   6.82685000e-02   7.54158800e-02   7.98840000e-02
           8.07953200e-02   7.92555800e-02   7.42452600e-02   6.66934000e-02
           5.76340800e-02   4.78272000e-02   3.81214600e-02   2.92280800e-02
           2.14393000e-02   1.52179600e-02   1.03508600e-02   6.73728000e-03
           4.23550000e-03   2.56246000e-03   1.47604000e-03   8.27220000e-04
           4.46880000e-04   2.29880000e-04   1.11100000e-04   5.39600000e-05]

    Further details:

    * http://docs.scipy.org/doc/numpy/release.html
    * https://github.com/numpy/numpy/issues/2012
    * https://github.com/numpy/numpy/pull/4687
    """
    rnd = np.random.RandomState(seed)
    rvs = rnd.binomial(nvec, pr, size=size)
    bins = np.arange(nvec + 2) - 0.5
    hist, bin_edges = np.histogram(rvs, bins=bins, density=True)
    exp = pr * nvec
    ix0 = np.floor(0.5 * exp)
    ix1 = np.ceil(1.5 * exp)

    if quiet:
        return hist

    # Print the NumPY version and PRNG output.
    print("NumPy version {}".format(np.__version__))
    print(hist[ix0:ix1])
