Log each pseudo-random number generator call
============================================

Welcome to the ``logrng`` documentation.
This package provides a wrapper for the NumPy pseudo-random number generator
that logs each method call.

.. _install:

Installation
------------

Clone the ``logrng`` repository and run:

.. code-block:: shell

   python setup.py install

If you don't have admin rights, install the package locally:

.. code-block:: shell

   python setup.py install --user

.. _start:

Getting Started
---------------

Replace instances of ``numpy.random.RandomState`` with
:py:class:`logrng.RandomState`, selecting an appropriate logging level:

.. code-block:: python

   >>> import logging
   >>> import logrng
   >>> r = logrng.RandomState(seed=1, level=logging.WARNING)
   >>> r.normal()
   WARNING:logrng.RandomState:normal()
   1.6243453636632417
   >>> r.binomial(n=10, p=0.25, size=5)
   WARNING:logrng.RandomState:binomial(p=0.25, size=5, n=10)
   WARNING:logrng.RandomState:[0, 2, ..., 1, 1]
   array([0, 2, 1, 1, 1])

.. class:: logrng.RandomState

   A pseudo-random number generator that logs each method call.

   .. automethod:: logrng.RandomState.__init__(seed=None, level=logging.DEBUG)

.. _test:

Comparing PRNG behaviours
-------------------------

The package also includes a convenience function for comparing the behaviour
of the pseudo-random number generator across multiple Python environments:

.. autofunction:: logrng.test_prng
